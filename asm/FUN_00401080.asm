section .text

; org 0x00401080
global _FUN_00401080
_FUN_00401080:
    push    edi
    mov     edi, [esp + 0x10]
    sub     edi, 0x1
    js      LAB_004010ae
    push    ebx
    mov     ebx, [esp + 0x18]
    push    ebp
    mov     ebp, [esp + 0x14]
    push    esi
    mov     esi, [esp + 0x14]
    lea     esp, [esp]
LAB_004010a0:
    mov     ecx, esi
    call    ebx
    add     esi, ebp
    sub     edi, 0x1
    jns     LAB_004010a0
    pop     esi
    pop     ebp
    pop     ebx
LAB_004010ae:
    pop     edi
    ret 0x10
