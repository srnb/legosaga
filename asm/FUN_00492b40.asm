section .text

; org 0x00492b40
global _FUN_00492b40
_FUN_00492b40:
    ; sub esp, 0xc
    ; push ebx
    ; push ebp
    ; push esi
    ; push edi
    ; lea eax, [esp + 0x24]
    ; push eax
    ; lea ecx, [esp + 0x24]
    ; push ecx
    ; call _FUN_004f4060
    ; call _FUN_0055f430
    ; mov edx, [esp + 0x2c]
    ; mov eax, [esp + 0x28]
    ; push edx
    ; push eax
    ; call _FUN_004011e0
    ; mov ecx, [_DAT_00802364]
    ; xor ebx, ebx
    ; push ebx
    ; mov ebp, 0x1
    ; push ebp
    ; push LAB_00494250
    ; push ecx
    ; push 0x17
    ; push _DAT_007facf0
    ; call _FUN_00509620
    ; call _FUN_00509760
    ; call _FUN_0065f4f0
    ; call _FUN_004f0d50
    ; push ebx
    ; push _DAT_00924894
    ; push _s_Episode_I_0075addc
    ; mov [_DAT_0087b560], eax
    ; call _FUN_004fc2b0
    ; push ebx
    ; push _DAT_00924894
    ; push _s_Episode_II_0075adcc
    ; mov [_DAT_0087b564], eax
    ; call _FUN_004fc2b0
    ; add esp, 0x40
    ; push ebx
    ; push _DAT_00924894
    ; push _s_Episode_III_0075adbc
    ; mov [_DAT_0087b568], eax
    ; call _FUN_004fc2b0
    ; push ebx
    ; push _DAT_00924894
    ; push _s_Episode_IV_0075adac
    ; mov [_DAT_0087b56c], eax
    ; call _FUN_004fc2b0
    ; push ebx
    ; push _DAT_00924894
    ; push _s_Episode_V_0075ad9c
    ; mov [_DAT_0087b570], eax
    ; call _FUN_004fc2b0
    ; push ebx
    ; push _DAT_00924894
    ; push _s_Episode_VI_0075ad8c
    ; mov [_DAT_0087b574], eax
    ; call _FUN_004fc2b0
    ; mov edx, dword [_DAT_0087b560]
    ; push edx
    ; mov [_DAT_0087b578], eax
    ; call _FUN_004f0d40
    ; push ebp
    ; call _FUN_005dabd0
    ; push ebp
    ; call _FUN_005dabe0
    ; call _FUN_0043c830
    ; call _FUN_0062f970
    ; fld dword [_DAT_00751528]
    ; add esp, 0x38
    ; fstp dword [esp]
    ; call _FUN_00698a00
    ; call _FUN_00596530
    ; call _FUN_0054a5e0
    ; call _FUN_004feef0
    ; push 0x2
    ; call _FUN_00566290
    ; push _DAT_007ff950
    ; call _FUN_0054b540
    ; or edi, 0xffffffff
    ; push edi
    ; call _FUN_0054b580
    ; add esp, 0x10
    ; call _FUN_005476e0
    ; call _FUN_004ca5b0
    ; call _FUN_00547700
    ; cmp dword [_DAT_0093b358], ebx
    ; jz LAB_00492cff
    ; movzx eax, byte [_DAT_008668f0]
    ; mov dword [esp + 0x14], eax
    ; mov esi, dword [_PTR_DAT_00802c54]
    ; push ecx
    ret
