# legosaga

Matching decompilation project for LEGO® Star Wars™: The Complete Saga.
"Matching" means that all assembly must match when compiled with the original
compiler.

## preparing

Prerequisites:
- A Windows machine or VM (Wine does not work)
- A torrent client ([Transmission](https://transmissionbt.com/) is recommended)

### install xmake

Follow xmake's installation instructions located [here](https://xmake.io/#/guide/installation).

### install NASM

Download the latest stable version of NASM from [nasm.us](https://www.nasm.us/). Choose the
default installation. Once it's done installing, add `nasm.exe` to your path by searching for
"Edit environment variables for your account" in the Start Menu, selecting `PATH`, choosing `Edit`,
and adding the directory that contains `nasm.exe` (usually `%LocalAppData%\bin\NASM`).

Here is an example `PATH` from a recent install of xmake and a manual addition of NASM:

![NASM on PATH](doc/nasm_on_path.png)

### install Visual Studio 2005

Retrieve a torrent for Visual Studio 2005 from the Internet Archive
[here](https://archive.org/download/en_vs_2005_pro_dvd/en_vs_2005_pro_dvd_archive.torrent).
Mount it, and run `vs\autorun.exe`. There may be popups like so:

![Program Compatibility Assistant Popup](doc/program_compatibility_assistant.png)

Click "Run the program without getting help" to continue, the installer works
just fine on Windows 10. Once the installer launches, you will be presented
with three options:

![Visual Studio Setup Options](doc/visual_studio_setup_options.png)

Select the first to proceed through the installer. When asked whether you would
like a "Default", "Full", or "Custom" install, select the "Custom" option, and
install these targets:

![Visual Studio C++ Targets](doc/visual_studio_cpp_targets.png)

_Note: there may be a more minimal install possible, however there has not been one tested._

Continuing through and finishing the installation process should add a "Visual
Studio 2005 Command Prompt" to your application list.

## building

1. Open a PowerShell.
2. Navigate to the folder where you cloned the repository using `cd C:\path\to\legosaga`.
3. Configure xmake with `xmake f -m releasedbg`.
4. Build the project with `xmake`.

You will now find a `LEGOStarWarsSaga.exe` and a `LEGOStarWarsSaga.pdb` in `build\windows\x86\releasedbg`.

## faq

### can I run this without an original copy?

No. The game requires extra data files to run, which you can find in the same
folder as your original copy.

### will this work as a replacement for the Steam version?

Yes, this decompilation is modeled after the GOG version, which is a drop-in
replacement.

### can I compile this with a different Visual Studio if I don't care about matching?

Maybe. There are no guarantees that everything will work while there is still
unmatched assembly.

### can I compile this without a Visual Studio?

No. Compiling the executable requires an MSVC compiler and an installation of
the Windows SDK.
