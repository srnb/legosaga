#include <iostream>
#include <stdlib.h>
#include <pstdint.h>
#include <windows.h>
#include <objbase.h>

#define MAX_BUFFER 0x100

typedef int32_t UnkType;

HINSTANCE hInstance_00924880;
int nCmdShow_00924888;
STICKYKEYS stickyKeys_0082763c;
TOGGLEKEYS toggleKeys_00827644;
FILTERKEYS filterKeys_0082764c;
BOOL screenSaveActive_02975c80;

extern "C" {
extern UnkType DAT_02975ab0;
extern UnkType DAT_00827664;
extern HINSTANCE DAT_00924880;
extern int DAT_00924888;

void FUN_00401080(int32_t a, int32_t b, int32_t c, void (*f)(void));
void FUN_006dc7a0(char arg0);
void FUN_006e5f60();
void FUN_004f5250(int32_t arg0);
void FUN_006dcf30(PSTR lpCmdLine);
char FUN_006dd160(char arg0);
void FUN_00492b40(UnkType arg0, UnkType* arg1);
void FUN_006de060();
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, int nCmdShow) {
    char cVar1;
    HANDLE hThread;
    DWORD_PTR dwThreadAffinityMask;

    hInstance_00924880 = hInstance;
    nCmdShow_00924888 = nCmdShow;
    SystemParametersInfoA(SPI_GETSTICKYKEYS, sizeof(STICKYKEYS), &stickyKeys_0082763c, 0);
    SystemParametersInfoA(SPI_GETTOGGLEKEYS, sizeof(TOGGLEKEYS), &toggleKeys_00827644, 0);
    SystemParametersInfoA(SPI_GETFILTERKEYS, sizeof(FILTERKEYS), &filterKeys_0082764c, 0);
    FUN_006dc7a0(0);
    SystemParametersInfoA(SPI_GETSCREENSAVEACTIVE, 0, &screenSaveActive_02975c80, 2);
    SystemParametersInfoA(SPI_SETSCREENSAVEACTIVE, 0, NULL, 2);
    SetThreadExecutionState(ES_CONTINUOUS | ES_DISPLAY_REQUIRED | ES_SYSTEM_REQUIRED);
    FUN_006e5f60();
    CoInitializeEx(NULL, 2);
    FUN_004f5250(1);
    FUN_006dcf30(lpCmdLine);
    cVar1 = FUN_006dd160(1);
    if (cVar1 != '\0') {
        dwThreadAffinityMask = 1;
        hThread = GetCurrentThread();
        SetThreadAffinityMask(hThread, dwThreadAffinityMask);
        FUN_00492b40(DAT_00827664, &DAT_02975ab0);
    }
    FUN_006de060();
    return 0;
}
