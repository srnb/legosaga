-- add the default rules for the different compilation modes
add_rules("mode.debug", "mode.release", "mode.releasedbg")

-- create a default rule for matching compilation
rule("matching")
    before_load(function (target)
        -- Windows x86
        target:set("plat", "windows")
        target:set("arch", "x86")
    
        -- use MSVC and NASM
        target:set("toolchains", "nasm", "msvc", {vs = "2005"})
        
        -- set Windows Vista as the minimum Windows version
        target:add("defines", "_WIN32_WINNT=0x0600")
        
        -- link necessary system libraries
        target:add("syslinks", "user32", "ole32")
        
        -- statically link the C Runtime
        if is_mode("debug") then
            target:add("cxflags", "/MTd")
        else
            target:add("cxflags", "/MT")
        end
    end)

target("LEGOStarWarsSaga")
    set_kind("binary")
    
    -- only matching builds for now
    add_rules("matching")
    
    -- include pstdint to make up for VS 2005 not having stdint.h
    add_includedirs(
        "external/pstdint/include"
    )
    
    -- platform-independent source files
    add_files(
        "asm/data.asm",
        "asm/FUN_00401080.asm",
        "asm/FUN_00492b40.asm",
        "asm/FUN_004f5250.asm",
        "asm/FUN_006dc7a0.asm",
        "asm/FUN_006dcf30.asm",
        "asm/FUN_006dd160.asm",
        "asm/FUN_006de060.asm",
        "asm/FUN_006e5f60.asm",
        "src/main.cpp"
    )
